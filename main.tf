terraform {
  required_providers {
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = ">= 2.0.0"
    }
  }
}
provider "kubernetes" {
  config_path = "~/.kube/config"
}

resource "kubernetes_deployment" "front_end_deployment" {
  metadata {
    name = "frontend-deployment"
  }

  spec {
    replicas = 1
    selector {
      match_labels = {
        app = "frontend"
      }
    }
    template {
      metadata {
        name = "frontend"
        labels = {
          app = "frontend"
        }
      }
      spec {
        container {
          name = "frontend-container"
          image = "stevenli3/producthunt-front:v1"
          image_pull_policy = "Always"
          port {
            container_port = 80
          }
        }
      }
    }
  }
}

resource "kubernetes_service" "front_end_service" {
  metadata {
    name = "frontend-service"
  }
  
  spec {
    selector = {
      app = "frontend"
    }
    port {
      protocol = "TCP"
      port = 80
      target_port = 80
    }
    type = "LoadBalancer"
  }
}

resource "kubernetes_deployment" "backend_deployment" {
  metadata {
    name = "backend"
  }

  spec {
    replicas = 1
    selector {
      match_labels = {
        app = "producthunt-api"
        tier = "backend"
      }
    }
    template {
      metadata {
          labels = {
            app = "producthunt-api"
            tier = "backend"
        }
      }
      spec {
        container {
          image = "stevenli3/producthunt-api:v1"
          name  = "backend-container"
          image_pull_policy = "Always"
          port {
            name = "backend-port"
            container_port = 3000
          }
        }
      }
    }
  }
}


resource "kubernetes_service" "back_end_service" {
  metadata {
    name = "producthunt-api"
  }
  
  spec {
    selector = {
      app = "producthunt-api"
      tier = "backend"
    }
    port {
      protocol = "TCP"
      port = 80
      target_port = "backend-port"
    }
  }
}